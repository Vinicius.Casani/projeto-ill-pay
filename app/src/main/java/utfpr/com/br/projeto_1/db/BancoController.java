package utfpr.com.br.projeto_1.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import utfpr.com.br.projeto_1.domain.Resultado;
import utfpr.com.br.projeto_1.domain.Usuario;

import static utfpr.com.br.projeto_1.db.CriaBanco.TABELA_RESULTADOS;
import static utfpr.com.br.projeto_1.db.CriaBanco.TABELA_USUARIO;

public class BancoController {

    private SQLiteDatabase db;
    private CriaBanco banco;

    public BancoController(Context context) {

        banco = new CriaBanco(context);
    }

    public String insereResultado(Resultado resultado) {

        ContentValues valores;
        long retorno;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(CriaBanco.DESCRICAO, resultado.getDescricao());
        valores.put(CriaBanco.MOEDA, resultado.getMoeda());
        valores.put(CriaBanco.VALOR, resultado.getValor());

        retorno = db.insert(TABELA_RESULTADOS, null, valores);
        db.close();

        if (retorno == -1)
            return "Erro ao inserir registro";
        else
            return "Registro inserido com sucesso";
    }

    public Cursor carregaDadosResultado() {

        Cursor cursor;

        String[] campos = {CriaBanco.ID, CriaBanco.DESCRICAO, CriaBanco.MOEDA, CriaBanco.VALOR, CriaBanco.DATA};

        db = banco.getReadableDatabase();
        cursor = db.query(TABELA_RESULTADOS, campos, null, null, null, null, CriaBanco.ID);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();

        return cursor;
    }

    public String insereUsuario(Usuario usuario) {

        ContentValues valores;
        long retorno;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(CriaBanco.NOME, usuario.getNome());
        valores.put(CriaBanco.PAIS_ORIGEM, usuario.getPaisOrigem());
        valores.put(CriaBanco.MOEDA_ORIGEM, usuario.getMoedaOrigem());

        retorno = db.insert(TABELA_RESULTADOS, null, valores);
        db.close();

        if (retorno == -1)
            return "Erro ao inserir registro";
        else
            return "Registro inserido com sucesso";
    }

    public Cursor carregaDadosUsuario() {

        Cursor cursor;

        String[] campos = {CriaBanco.ID, CriaBanco.NOME, CriaBanco.MOEDA_ORIGEM, CriaBanco.PAIS_ORIGEM};

        db = banco.getReadableDatabase();
        cursor = db.query(TABELA_USUARIO, campos, null, null, null, null, CriaBanco.ID);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        db.close();

        return cursor;
    }

    /*public String alteraDado(String id, String titulo, String autor, String editora) {
        ContentValues valores;
        String where;

        db = banco.getWritableDatabase();

        where = CriaBanco.ID + "=?";
        String[] args = new String[]{id};

        valores = new ContentValues();
        valores.put(CriaBanco.TITULO, titulo);
        valores.put(CriaBanco.AUTOR, autor);
        valores.put(CriaBanco.EDITORA, editora);

        long resultado = db.update(CriaBanco.TABELA, valores, where, args);
        db.close();

        if (resultado == -1)
            return "Erro ao alterar registro";
        else
            return "Registro alterado com sucesso";
    }*/
/*
    public String excluiRegistro(String idString) {

        String where = banco.ID + "=?";

        db = banco.getReadableDatabase();
        long resultado = db.delete(banco.TABELA, where, new String[]{idString});
        db.close();

        if (resultado == -1)
            return "Erro ao excluir registro";
        else
            return "Registro excluído com sucesso";
    }*/
}
