package utfpr.com.br.projeto_1.service;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import utfpr.com.br.projeto_1.domain.Cotacao;

public class HttpService extends AsyncTask<Void, Void, Cotacao> {

    private final String moeda;

    public HttpService(String moeda) {

        this.moeda = moeda;
    }

    @Override
    protected Cotacao doInBackground(Void... voids) {
        StringBuilder resposta = new StringBuilder();

        if (this.moeda != null) {
            try {
                URL url = new URL("http://economia.awesomeapi.com.br/" + this.moeda + "/1");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);

                connection.connect();

                Scanner scanner = new Scanner(url.openStream());
                while (scanner.hasNext()) {
                    resposta.append(scanner.next());
                }

            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
        Type listType = new TypeToken<ArrayList<Cotacao>>(){}.getType();
        List<Cotacao> yourClassList = new Gson().fromJson(resposta.toString(), listType);
        return yourClassList.get(0);

    }
}
