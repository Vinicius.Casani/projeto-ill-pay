package utfpr.com.br.projeto_1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

import utfpr.com.br.projeto_1.R;
import utfpr.com.br.projeto_1.domain.Cotacao;
import utfpr.com.br.projeto_1.service.HttpService;

public class ConversaoActivity extends AppCompatActivity {

    private String moeda_origem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversao);

        Bundle args = getIntent().getExtras();
        moeda_origem = args.getString("moeda_origem");

        TextView textView = findViewById(R.id.lblValorOriginal);
        textView.setText("Informe o valor do produto em: " + moeda_origem);

        Spinner spinner = findViewById(R.id.moedas);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.moedas_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

    }

    public void calcularConversao(View view) {
        TextView txtValorConverter = findViewById(R.id.txtValor);
        TextView txtValorOriginal = findViewById(R.id.txtValorOriginal);
        BigDecimal valorResultado = new BigDecimal(0.0);
        BigDecimal valorOriginal;

        if (txtValorOriginal.getText().toString().trim().equals("")) {
            txtValorOriginal.setError("Valor é obrigatório");
        } else if (txtValorConverter.getText().toString().trim().equals("")) {
            txtValorConverter.setError("Valor é obrigatório");
        } else {

            valorOriginal = new BigDecimal(txtValorOriginal.getText().toString());

            Spinner txtMoeda = findViewById(R.id.moedas);
            String moeda = txtMoeda.getSelectedItem().toString();

            try {
                Cotacao cotacao = new HttpService(moeda).execute().get();
                BigDecimal valorConverter = new BigDecimal(txtValorConverter.getText().toString());
                BigDecimal valorCotacao = new BigDecimal(cotacao.getAsk());

                valorCotacao = valorCotacao.setScale(2, BigDecimal.ROUND_HALF_UP);
                valorResultado = valorConverter.multiply(valorCotacao).setScale(2, BigDecimal.ROUND_HALF_EVEN);

            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(this, ResultadoActivity.class);
            Bundle params = new Bundle();

            params.putString("valorResultado", String.valueOf(valorResultado));
            params.putString("valorOriginal", String.valueOf(valorOriginal));
            params.putString("moeda", moeda);
            params.putString("moeda_origem", moeda_origem);
            intent.putExtras(params);
            startActivity(intent);

        }

    }

}
