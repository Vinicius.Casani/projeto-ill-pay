package utfpr.com.br.projeto_1.domain;

import java.io.Serializable;
import java.time.LocalDate;

public class Resultado implements Serializable {

    private String descricao;
    private String moeda;
    private String valor;
    private LocalDate data;

    public Resultado(String descricao, String moeda, String valor, LocalDate data) {
        this.descricao = descricao;
        this.moeda = moeda;
        this.valor = valor;
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Nome: " + descricao +
                "\nMoeda: " + moeda +
                "\nValor: " + valor + "\nData: " + data;
    }
}
