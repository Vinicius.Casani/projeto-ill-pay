package utfpr.com.br.projeto_1.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CriaBanco extends SQLiteOpenHelper {

    private static final String NOME_BANCO = "banco.db";

    public static final String TABELA_RESULTADOS = "resultados";
    public static final String ID = "_id";
    public static final String DESCRICAO = "descricao";
    public static final String MOEDA = "moeda";
    public static final String VALOR = "valor";
    public static final String DATA = "data";

    public static final String TABELA_USUARIO = "usuario";
    public static final String NOME = "nome";
    public static final String PAIS_ORIGEM = "pais";
    public static final String MOEDA_ORIGEM = "moeda_origem";


    private static final int VERSAO = 1;

    public CriaBanco(Context context) {
        super(context, NOME_BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String sql_tabela_resultados = "CREATE TABLE " + TABELA_RESULTADOS + "("
                + ID + " integer primary key autoincrement,"
                + DESCRICAO + " text,"
                + MOEDA + " text,"
                + VALOR + " text,"
                + DATA + " datetime default current_timestamp"
                + ")";

        sqLiteDatabase.execSQL(sql_tabela_resultados);

        String sql_tabela_usuario = "CREATE TABLE " + TABELA_USUARIO + "("
                + ID + " integer primary key autoincrement,"
                + NOME + " text,"
                + MOEDA + " text,"
                + PAIS_ORIGEM + " text,"
                + MOEDA_ORIGEM + "text"
                + ")";

        sqLiteDatabase.execSQL(sql_tabela_usuario);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABELA_RESULTADOS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABELA_USUARIO);
        onCreate(sqLiteDatabase);
    }
}
