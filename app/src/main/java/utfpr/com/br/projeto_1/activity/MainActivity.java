package utfpr.com.br.projeto_1.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import utfpr.com.br.projeto_1.R;
import utfpr.com.br.projeto_1.db.BancoController;
import utfpr.com.br.projeto_1.db.CriaBanco;
import utfpr.com.br.projeto_1.domain.Usuario;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner spinner = findViewById(R.id.moedas_origem);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.moedas_origem_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }

    public void enviarInfo(View view) {

        TextView txtNomeUsuario = findViewById(R.id.txtNome);

        String nomeUsuario = txtNomeUsuario.getText().toString();

        Spinner txtMoeda = findViewById(R.id.moedas_origem);
        String moeda = txtMoeda.getSelectedItem().toString();

        Intent intent = new Intent(this, ConversaoActivity.class);
        Bundle params = new Bundle();

        params.putString("nome", nomeUsuario);
        params.putString("moeda_origem", moeda);
        intent.putExtras(params);
        startActivity(intent);
    }
}
