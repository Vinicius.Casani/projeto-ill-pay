package utfpr.com.br.projeto_1.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import utfpr.com.br.projeto_1.R;
import utfpr.com.br.projeto_1.db.BancoController;
import utfpr.com.br.projeto_1.db.CriaBanco;

public class UltimasConversoesActivity extends AppCompatActivity {

    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ultimas_conversoes);

        BancoController bancoController = new BancoController(getBaseContext());
        final Cursor cursor = bancoController.carregaDadosResultado();

        String[] campos = { CriaBanco.DESCRICAO, CriaBanco.VALOR, CriaBanco.MOEDA, CriaBanco.DATA};
        int[] idViews = new int[]{ R.id.descricao, R.id.valor, R.id.lblMoeda, R.id.data};

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(getBaseContext(), R.layout.list_resultados_adapter, cursor,
                campos, idViews, 0);

        lista = findViewById(R.id.lista);
        lista.setAdapter(adapter);
    }

    public void voltarParaConversao(View view) {

        Intent intent = new Intent(this, ConversaoActivity.class);

        startActivity(intent);
    }
}
