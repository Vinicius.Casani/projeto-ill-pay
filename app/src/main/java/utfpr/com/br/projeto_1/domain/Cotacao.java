package utfpr.com.br.projeto_1.domain;

public class Cotacao {

    private String code;
    private String codeIn;
    private String name;

    //valor da cotação
    private String ask;

    @Override
    public String toString() {
        return "Cotacao{" +
                "code='" + code + '\'' +
                ", codeIn='" + codeIn + '\'' +
                ", name='" + name + '\'' +
                ", ask='" + ask + '\'' +
                '}';
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeIn() {
        return codeIn;
    }

    public void setCodeIn(String codeIn) {
        this.codeIn = codeIn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }
}
