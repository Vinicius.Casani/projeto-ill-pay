package utfpr.com.br.projeto_1.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;
import java.time.LocalDate;

import utfpr.com.br.projeto_1.R;
import utfpr.com.br.projeto_1.db.BancoController;
import utfpr.com.br.projeto_1.domain.Resultado;

public class ResultadoActivity extends AppCompatActivity {

    private static BigDecimal valorResultado;
    private static String moeda;
    private BigDecimal valorOriginal;
    String m_Text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        Bundle args = getIntent().getExtras();
        moeda = args.getString("moeda");
        String moeda_origem = args.getString("moeda_origem");
        valorResultado = new BigDecimal(args.getString("valorResultado"));
        valorOriginal = new BigDecimal(args.getString("valorOriginal"));

        TextView lblValorConvertido = findViewById(R.id.lblValorConvertido);
        lblValorConvertido.setText("Valor convertido do produto de "+ moeda+ " para " + moeda_origem);

        String feedback;

        if (valorResultado.compareTo(valorOriginal) <= 0) {
            feedback = "Realizar a compra deste produto em " + moeda+ " é uma boa escolha.\n" +
                    "\nVocê ganhará: " + valorOriginal.subtract(valorResultado);
        } else {
            feedback = "Não é viável realizar a compra deste produto em " + moeda + ".\n" +
                    "\nVocê perderá: " + valorResultado.subtract(valorOriginal);
        }

        TextView textView = findViewById(R.id.txtValorConvertido);
        textView.setText(String.valueOf(valorResultado));

        TextView txtFeedback = findViewById(R.id.txtFeedback);
        txtFeedback.setText(feedback);

    }

    private void gravarResultado(String nomeProduto) {

        BancoController bancoController = new BancoController(getBaseContext());

        Resultado resultado = new Resultado(nomeProduto, moeda, String.valueOf(valorResultado), LocalDate.now());

        bancoController.insereResultado(resultado);

    }

    public void voltarParaConversao(View view) {

        Intent intent = new Intent(this, ConversaoActivity.class);

        startActivity(intent);
    }

    public void salvarConversao(View view) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Salvar Conversão");

        builder.setMessage(R.string.nome_produto);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                m_Text = input.getText().toString();
                prosseguir();
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    private void prosseguir() {
        gravarResultado(m_Text);

        Intent intent = new Intent(this, UltimasConversoesActivity.class);

        startActivity(intent);

    }
}